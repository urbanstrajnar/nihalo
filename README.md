# Projekt: Dvojno nihalo #

## Osnovni podatki ##

* Urban Strajnar
* git repo: https://bitbucket.org/urbanstrajnar/nihalo

##  Osnovna verzija ##
Program simulira nihanje dvojnega nihala


Program prikazuje animacijo nihala na zaslonu.

## Razširitve ##

* Uporabnik lahko z miško nastavi lego nihala.
* Z drsniki lahko nastavi različne parametre, kakor tudi lego in hitrost nihala
* Dušeno nihanje