/**
 * 
 */
package nihalo.main;


import java.util.Vector;

import nihalo.okno.MyMouseListener;
import nihalo.okno.Okno;
import nihalo.risanje.Nihalo;

/**
 * @author Urban Strajnar
 *
 */
public class Main{

	/**
	 * @param args
	 */
	

	public static void main(String[] args) {
		
		//Casovni zamik med posameznimi koraki simulacije
		int delay = 10;

		Okno okno = new Okno("Simulacija dvojnega nihala");		
		Vector<Nihalo> nihala = new Vector<Nihalo>();
		
		nihala.add(new Nihalo(0, 10, 50, 100));
		nihala.add(new Nihalo(1, 10, 50, 100));
		
		
		okno.getPlatno().dodajNihalo(nihala.firstElement());
		okno.getPlatno().dodajNihalo(nihala.lastElement());
		
		MyMouseListener mml = new MyMouseListener(okno.getPlatno());
		
		okno.getPlatno().addMouseListener(mml);
		okno.getPlatno().addMouseMotionListener(mml);
		
		//razred, ki resuje diferencialne enacbe za nihalo
		Racunanje rac = new Racunanje(okno.getPlatno(), 1, 0.001 * delay);
		// razred, ki skrbi za animacijo nihala
		Stevec stevec = new Stevec(delay, rac);
		
		
		
		stevec.getTimer().start();

	}

}
