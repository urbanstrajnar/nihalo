package nihalo.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 * @author Urban
 * Razred, ki skrbi za izvajanje animacije in racunanje
 * na vsakih "delay" milisekund.
 */
public class Stevec implements ActionListener {
	
	Timer tm;
	Racunanje rac;
	
	public Stevec(int delay, Racunanje rac){
		super();
		tm = new Timer(delay, this);
		this.rac = rac;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		rac.korak();
		
	}

	public Timer getTimer() {
		// TODO Auto-generated method stub
		return tm;
	}

}
