package nihalo.main;

import nihalo.okno.Drsnik;
import nihalo.okno.Platno;
import nihalo.risanje.Nihalo;

/**
 * @author Urban
 * Razred, ki skrbi za numericno resevanje diferencialnih enacb,
 * skrbi za ujemanje podatkov na drsnikih in nihalu, ter racuna skupno energijo,
 * (ki se je nikjer ne vidi).
 * 
 */
public class Racunanje {
	
	private Platno platno;
	private Nihalo nihalo;
	//pove koliko nihal imamo, stevilo nihal = red + 1.
	private int red;
	private double[] enacbe;
	private double[] enacbe1;
	private double[] dolzina;
	private double[] masa;
	private double g = 9.8065;
	//Koeficienti za Runge-Kutta
	private double[] k1;
	private double[] k2;
	private double[] k3;
	private double[] k4;
	//Racunski korak, delta t.
	private double h;
	//Seznam diferencialnih enacb.
	private double[] dydx;
	//Stevilo diferencialnih enacb
	private int N;
	//Pove, ali je trenutno pavza, ali se animacija izvaja.
	private boolean play;
	//Pove, katero metodo uporabljamo za numericno racunanje(Runge-Kutta = 0, Euler = 1).
	private int metoda;


	public Racunanje(Platno platno, int red, double h) {
		super();
		this.h = h;
		N = 2*(red+1);
		this.platno = platno;
		this.red = red;
		enacbe = new double[N];
		enacbe1 = new double [N];
		dolzina  = new double[red+1];
		masa = new double[red+1];
		k1 = new double[N];
		k2 = new double[N];
		k3 = new double[N];
		k4 = new double[N];
		dydx =new double[N];
		nastavi();
		metoda = platno.getIzbiraMetode().getMetoda();
	}
	
	
	//glavna metoda, ki se izvede na vsakem "koraku" stevca.
	public void korak(){
		nastavi();
		racunajEnergijo();
		
		if(play){
			metoda = platno.getIzbiraMetode().getMetoda();
				
			if(metoda == 0){
				racunajNumRunge();
			}
			else{
				racunajNumEuler();
			}
				
			nastaviNove();
			
		}
		
		platno.repaint();
	}
	
	//Metoda, ki zbere potrebne podatke, za integriranje diferencialnih enacb
	//Ter poskrbi, da se podatki osvezijo, kadar premikamo drsnike in obratno.
	private void nastavi(){
		double skrcitev = 100.0;
		play = platno.getPavza().getPlay();
		posodobiDrsnike();
		
		for(int i = 0; i < red + 1; i++){
			nihalo = platno.getNihala().elementAt(i);
			if(play){
				nihalo.povecajStevec();
				nihalo.setTocke();
			}
			enacbe[2*i] = nihalo.getTheta();
			enacbe1[2*i] = nihalo.getTheta();
			enacbe[2*i+1] = nihalo.getOmega();
			enacbe1[2*i+1] = nihalo.getOmega();
			dolzina[i] = nihalo.getDolzina()/skrcitev;
			masa[i] = nihalo.getMasa();
			if(i == 0){
				platno.getSubPlatno1().nastaviVrednosti(nihalo);
			}
			else{
				platno.getSubPlatno2().nastaviVrednosti(nihalo);
			}
					
		}
		
	}
	
	//Racuna numericne vrednosti diferencialnih enacb
	private void odvodi(double[] enacbe){
		double[] theta = new double[red + 1];
		double[] omega = new double[red + 1];
		if(red == 1){
			theta[0] = enacbe[0];
			theta[1] = enacbe[2];
			omega[0] = enacbe[1];
			omega[1] = enacbe[3];
			
			dydx[0] = enacbe[1];
			
			/*dydx[1] = (masa[1]*dolzina[0]*omega[0]*omega[0]*Math.sin(delta)*Math.dolzina[0](delta)
					+ masa[1]*g*Math.sin(theta[1])*Math.cos(delta)
					+ masa[1]*dolzina[1]*omega[1]*omega[1]*Math.sin(delta)
					-(masa[0]+masa[1])*Math.sin(theta[0]))
					/((masa[0] + masa[1])*dolzina[0] - masa[1]*dolzina[0]*(Math.cos(delta)*Math.cos(delta)));*/
			
			dydx[1] = (-g*(2*masa[0] + masa[1])*Math.sin(enacbe[0]) -
		            masa[1]*g*Math.sin(enacbe[0]-2*enacbe[2]) -
		            2*Math.sin(enacbe[0] - enacbe[2])*masa[1]*(enacbe[3]*enacbe[3] * dolzina[1] +
		            enacbe[1]*enacbe[1]*dolzina[0] * Math.cos(enacbe[0] - enacbe[2])))/(dolzina[0]*(2*masa[0] + masa[1] -masa[1]*Math.cos(2*enacbe[0]-2*enacbe[2])));
			
			dydx[2] = enacbe[3];
			
	/*		dydx[3] =  (-masa[1]*dolzina[1]*omega[1]*omega[1]*Math.sin(delta)*Math.cos(delta)
					+ (masa[0] + masa[1])*(g*Math.sin(theta[0])*Math.cos(delta)
							- dolzina[0]*omega[0]*omega[0]*Math.sin(delta)
					-g*Math.sin(theta[1])))
					/((masa[0] + masa[1])*dolzina[1] - masa[1]*dolzina[1]*(Math.cos(delta)*Math.cos(delta)));*/
			
			dydx[3] =  (2*Math.sin(enacbe[0] - enacbe[2])*(enacbe[1]*enacbe[1]*dolzina[0]*(masa[0] + masa[1]) +
		            g*(masa[0] + masa[1])*Math.cos(enacbe[0]) +
		            enacbe[3]*enacbe[3]*dolzina[1]*masa[1]*Math.cos(enacbe[0] - enacbe[2])))/(dolzina[1]*(2*masa[0] + masa[1] -masa[1]*Math.cos(2*enacbe[0]-2*enacbe[2])));
			
		}
		//Ostanki enacb za enojno nihalo
		else{
			dydx[0] = enacbe[1];
			dydx[1] = (g/dolzina[0]) * Math.sin(enacbe[0]);
			
		}
		
		return;
	}
	
	//Metoda Runge-Kutta
	private void racunajNumRunge(){
		odvodi(enacbe);
		

		for(int i = 0; i < N; i++){
			
			k1[i] = h*dydx[i];
			enacbe1[i] = enacbe[i] + 0.5*k1[i];
		}
		
		odvodi(enacbe1);
		for(int i = 0; i < N; i++){
			
			k2[i] = h*dydx[i];
			enacbe1[i] = enacbe[i] + 0.5*k1[i];
		}
		odvodi(enacbe1);
		for(int i = 0; i < N; i++){
			
			k3[i] = h*dydx[i];
			enacbe1[i] = enacbe[i] + 0.5*k1[i];
		}
		
		odvodi(enacbe1);
		for(int i = 0; i < N; i++){
			
			k4[i] = h*dydx[i];
			enacbe[i] = enacbe[i] + k1[i]/6.0 + k2[i]/3.0 + k3[i]/3.0 + k4[i]/6.0;
			

		}
		
		//Dusenje nihal
		double k = 0.001;		
		enacbe[1] = enacbe[1] -k*enacbe[1];
		enacbe[3] = enacbe[3] -k*enacbe[3];
		
		urediKot();
		
	}
	//Eulerjeva metoda
	private void racunajNumEuler(){
		odvodi(enacbe);
		for(int  i = 0; i < N; i++){
			enacbe[i] = enacbe[i] + h*dydx[i];
		}
		//Dusenje nihal
		double k = 0.001;		
		enacbe[1] = enacbe[1] -k*enacbe[1];
		enacbe[3] = enacbe[3] -k*enacbe[3];
				
		urediKot();
		
	}
	
	//Obdrzi kot med -180 in 180 stopinj
	private void urediKot(){
		if(enacbe[0] > Math.PI){
			
			enacbe[0] -= 2*Math.PI;
		}
		else if(enacbe[0] < -Math.PI){
			
			enacbe[0] += 2*Math.PI;
		}
		if(enacbe[2] > Math.PI){
			
			enacbe[2] -= 2*Math.PI;
			
		}
		else if(enacbe[2] < -Math.PI){
			
			enacbe[2] += 2*Math.PI;		
		}
	}
	
	
	//Posodobi nihala na nove vrednosti iz numericnih resitev.
	private void nastaviNove(){
		
		for(int i = 0; i < N; i++){
			if(i % 2 == 0){
				platno.getNihala().elementAt(i/2).setTheta(enacbe[i]);
			}
			else{
					
				platno.getNihala().elementAt(i/2).setOmega(enacbe[i]);
			}
		}
		if(red == 1){
			platno.getNihala().elementAt(1).setxPos(platno.getNihala().elementAt(0).getxCenter());
			platno.getNihala().elementAt(1).setyPos(platno.getNihala().elementAt(0).getyCenter());
		}	
	}
	
	//Med pavzo spreminja nihala glede na lego drsnikov,
	//med animacijo pa spreminja drsnike, glede na lego nihal.
	public void posodobiDrsnike(){
		
		Nihalo nihalo1 = platno.getNihala().elementAt(0);
		Nihalo nihalo2 = platno.getNihala().elementAt(1);
		
		for(int i = 0; i < 5; i++){
			Drsnik drsnik1 = platno.getSubPlatno1().getDrsniki().elementAt(i);
			Drsnik drsnik2 = platno.getSubPlatno2().getDrsniki().elementAt(i);
			
			if(play || nihalo1.pritisnjen || nihalo2.pritisnjen){
				switch(i){
				case 0: drsnik1.setValue((int) (180*nihalo1.getTheta()/Math.PI));
						drsnik2.setValue((int) (180*nihalo2.getTheta()/Math.PI));
						break;
				case 1: drsnik1.setValue((int) (5.0*nihalo1.getOmega()));
						drsnik2.setValue((int) (5.0*nihalo2.getOmega()));
						break;
				case 2: drsnik1.setValue((int) (nihalo1.getDolzina()));
						drsnik2.setValue((int) (nihalo2.getDolzina()));
						break;
				case 3: drsnik1.setValue((int) (nihalo1.getR()));
						drsnik2.setValue((int) (nihalo2.getR()));
						break;
				case 4: drsnik1.setValue((int) (nihalo1.getMasa()));
						drsnik2.setValue((int) (nihalo2.getMasa()));
						break;

						
				}
			}
			else{
				switch(i){
				case 0: nihalo1.setTheta((Math.PI * (double)drsnik1.getValue()/180.0));
						
						nihalo2.setTheta((Math.PI * (double)drsnik2.getValue()/180.0));
						
						break;
				case 1: nihalo1.setOmega((double)drsnik1.getValue()/5.0);
				
						nihalo2.setOmega((double)drsnik2.getValue()/5.0);
				break;
				case 2: nihalo1.setDolzina((double)drsnik1.getValue());
				
						nihalo2.setDolzina((double)drsnik2.getValue());
				break;
				case 3: nihalo1.setR((double)drsnik1.getValue());
				
						nihalo2.setR((double)drsnik2.getValue());
				break;
				case 4: nihalo1.setMasa((double)drsnik1.getValue());
				
						nihalo2.setMasa((double)drsnik2.getValue());
						break;
				
				}
				nihalo2.setxPos(nihalo1.getxCenter());
				nihalo2.setyPos(nihalo1.getyCenter());
			}
		}
	}
	
	//Izracuna skupno energijo dvojnega nihala.
	private double racunajEnergijo(){
		double lt1 = enacbe[1]*dolzina[0];
		double lt2 = enacbe[3]*dolzina[1];
		double E = 0.5*(masa[0]+masa[1])*lt1*lt1 
				+ 0.5*masa[1]*lt2*lt2 + masa[1]*lt1*lt2*Math.cos(enacbe[2]-enacbe[0])
				+(masa[0]+masa[1])*g*dolzina[0]*(1-Math.cos(enacbe[0])) + masa[1]*g*dolzina[1]*(1-Math.cos(enacbe[2]));
		//System.out.println(E);
		return E;
	}
		
}
