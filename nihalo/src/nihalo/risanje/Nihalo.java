package nihalo.risanje;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author Urban
 * Razred nihalo vsebuje vse podatke o nihalu in o njegovem risanju.
 * Vsebuje tudi seznam tock za risanje sledi po zaslonu.
 */
public class Nihalo {

	private double r;
	private double masa;
	private double dolzina;
	private double theta;
	private double omega;
	//To so koordinate tocke, kjer je nihalo pripeto.
	private double xPos;
	private double yPos;
	private int red;
	//To so koordinate tocke, kjer je sredisce nihala.
	private double xCenter;
	private double yCenter;
	public boolean pritisnjen;
	
	//Spremenljivke za risanje sledi.
	private double[] xTocke;
	private double[] yTocke;
	private int stevec;
	private boolean celKrog;
	//Stevilo tock, za risanje sledi.
	private int tocke;
	
	public Nihalo(int red, double r, double masa, double dolzina) {
		super();
		celKrog = false;
		stevec = 0;
		tocke = 250;
		this.red = red;
		this.r = r;
		this.masa = masa;
		this.dolzina = dolzina;
		xCenter = xPos + Math.sin(theta) * dolzina;
		yCenter = yPos + Math.cos(theta) * dolzina;
		pritisnjen = false;
		xTocke = new double[tocke];
		yTocke = new double[tocke];
	}

	
	// metoda, ki zna narisati kroglo nihala
	public void narisiKroglo(Graphics g) {
		if(red == 0){
			g.setColor(Color.green);
		}
		else{
			g.setColor(Color.red);
		}
		g.fillOval((int)(xCenter- r),
				(int) (yCenter - r), (int) (2 * r),
				(int) (2 * r));
	}
	// metoda, ki zna narisati vrvico nihala
	public void narisiVrvico(Graphics g) {
		g.setColor(Color.black);
		g.drawLine((int)xPos, (int)yPos,(int) xCenter, (int)yCenter);
	}
	
	
	//metoda, ki resetira crto
	
	public void resetCrta(){
		stevec = 0;
		celKrog = false;
	}
	
	// metoda, ki rise sled za nihalom.
	public void narisiCrto(Graphics g){
		if(red == 0){
			g.setColor(Color.green);
		}
		else{
			g.setColor(Color.red);
		}
		if(!celKrog && stevec > 2){
			for(int i = 1; i < stevec; i++){
				g.drawLine((int) xTocke[i],(int) yTocke[i],(int) xTocke[i+1],(int)yTocke[i+1]);
			}
		}
		else if(celKrog){
			int i = stevec;
			for(int k = 0; k < tocke; k++){
				if(i != stevec){
					if(i == tocke - 1){
						g.drawLine((int) xTocke[i],(int) yTocke[i],(int) xTocke[0],(int)yTocke[0]);
					}
					else{
						g.drawLine((int) xTocke[i],(int) yTocke[i],(int) xTocke[i+1],(int)yTocke[i+1]);						
					}			
				}
				i++;
				if(i > tocke - 1){
					i = 0;
				}
			}
		}
	}
	
	// getterji in setterji
	public double getMasa() {
		return masa;
	}

	public void setMasa(double masa) {
		this.masa = masa;
	}

	public double getDolzina() {
		return dolzina;
	}

	public void setDolzina(double dolzina) {
		this.dolzina = dolzina;
		yCenter = (yPos + Math.cos(theta) * dolzina );
		xCenter = (xPos + Math.sin(theta) * dolzina );
	}

	public double getTheta() {
		return theta;
	}

	public void setTheta(double theta) {
		this.theta = theta;
		yCenter = (yPos + Math.cos(theta) * dolzina );
		xCenter = (xPos + Math.sin(theta) * dolzina );
	}

	public double getOmega() {
		return omega;
	}

	public void setOmega(double omega) {
		this.omega = omega;
	}
	public double getxPos() {
		return xPos;
	}

	public void setxPos(double xPos) {
		this.xPos = xPos;
		xCenter = (xPos + Math.sin(theta) * dolzina);
	}

	public double getyPos() {
		return yPos;		
	}

	public void setyPos(double yPos) {
		this.yPos = yPos;
		yCenter = (yPos + Math.cos(theta) * dolzina);
	}
	
	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
		
	}
	public double getxCenter() {
		return xCenter;
	}


	public void setxCenter(double xCenter) {
		this.xCenter = xCenter;
	}

	public double getyCenter() {
		return yCenter;
	}

	public void setyCenter(double yCenter) {
		this.yCenter = yCenter;
	}

	public int getRed() {
		return red;
	}
	
	//metode za risanje sledi
	public int getStevec() {
		return stevec;
	}

	public void povecajStevec() {
		stevec += 1;
		if(stevec >= tocke){
			stevec = 0;
			celKrog = true;
		}
	}

	public void setTocke() {
		xTocke[stevec] = xCenter;
		yTocke[stevec] = yCenter;
	}

}
