package nihalo.okno;

import javax.swing.JSlider;


/**
 * @author Urban
 * Razred Drsnik predstavlja en drsnik, ki hrani podatke o enem parametru nihala.
 */
@SuppressWarnings("serial")
public class Drsnik extends JSlider{

	public Drsnik(int arg0) {
		super(arg0);
		setOpaque(true);
		setPaintTicks(true);
		setPaintTrack(true);
		setMajorTickSpacing(10);
		setMinorTickSpacing(1);
		
	}
	
	
	public void nastaviMeje(int min, int max){
		setMinimum(min);
		setMaximum(max);
		
	}
	public void toggle() {
		if(isEnabled()){
			setEnabled(false);
		}
		else{
			setEnabled(true);
		}
		
	}
	
}
