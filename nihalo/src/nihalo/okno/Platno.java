package nihalo.okno;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Vector;
import javax.swing.JPanel;

import nihalo.risanje.Nihalo;

/**
 * @author Urban
 * Razred platno je podrocje za risanje, 
 * ter vsebuje komponente kot so, SubPlatno in Pavza.
 */
@SuppressWarnings("serial")
public class Platno extends JPanel {
	
	private Vector<Nihalo> nihala;
	private Pavza pavza;
	private Restart restart;
	private SubPlatno subPlatno1;
	private SubPlatno subPlatno2;
	private IzbiraMetode izbiraMetode;

	public Platno() {
		 super();
		 this.nihala = new Vector<Nihalo>();
		 setBackground(Color.white);
		 pavza = new Pavza();
		 restart = new Restart(this);
		 izbiraMetode = new IzbiraMetode();
		 setLayout(null);
		 
		 
		 subPlatno1 = new SubPlatno(1);
		 subPlatno2 = new SubPlatno(2);
		 subPlatno1.setBounds(90, 325, 250, 150);
		 subPlatno2.setBounds(345, 325, 250, 150);
		 add(subPlatno1);
		 add(subPlatno2);
	}

	
	//Preden dodamo nihala, jim nastavimo parametre.
	public void dodajNihalo(Nihalo nihalo){
		if(nihalo.getRed() == 0){
			nihalo.setOmega(0);
			nihalo.setTheta(Math.PI/2.0+Math.random()*Math.PI);
			nihalo.setxPos(300);
			nihalo.setyPos(100);
		}
		else{
			nihalo.setOmega(0);
			nihalo.setTheta(Math.PI/2.0+Math.random()*Math.PI);
			nihalo.setxPos(nihala.firstElement().getxCenter());
			nihalo.setyPos(nihala.firstElement().getyCenter());
		}
		nihala.add(nihalo);
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(600, 480);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		pavza.narisi(g);
		restart.narisi(g);
		izbiraMetode.narisi(g);
		//ta for zanka  rise sled od nihal.
		for (Nihalo nihalo : this.nihala) {
			nihalo.narisiCrto(g);
		}
		
		for (Nihalo nihalo : this.nihala) {
			nihalo.narisiVrvico(g);
		}
		for (Nihalo nihalo : this.nihala) {
			nihalo.narisiKroglo(g);
		}
	}
	
	public Vector<Nihalo> getNihala(){
		return nihala;
	}

	public Pavza getPavza() {
		return pavza;
	}
	
	public Restart getRestart(){
		return restart;
	}
	public SubPlatno getSubPlatno1() {
		return subPlatno1;
	}


	public SubPlatno getSubPlatno2() {
		return subPlatno2;
	}


	public IzbiraMetode getIzbiraMetode() {
		return izbiraMetode;
	}

}
