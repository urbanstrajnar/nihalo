package nihalo.okno;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;

import nihalo.risanje.Nihalo;

/**
 * @author Urban
 * Razred, ki sluzi kot drzalo za drsnike in njihove napise.
 */
@SuppressWarnings("serial")
public class SubPlatno extends JPanel {
	
	private Vector<Drsnik> drsniki;
	

	private Vector<JLabel> vrednosti;
	private GridBagConstraints c;
	
	public SubPlatno(int nihalo) {
		super();
		c = new GridBagConstraints();
		drsniki = new Vector<Drsnik>();
		vrednosti = new Vector<JLabel>();
		
		
		setLayout(new GridBagLayout());
		nastaviNapise();
		nastaviVrednostiNapise();
		nastaviDrsnike();
		c.gridy = 0;
		c.gridx = 2;
		if(nihalo == 1){
			add(new JLabel("Nihalo 1"), c);
			setBackground(Color.green);
		}
		else{
			add(new JLabel("Nihalo 2"), c);
			setBackground(Color.red);
		}
		
	}
	
	//Dodamo drsnike in jim povemo najvecje in najmanjse vrednosti.
	public void nastaviDrsnike(){
		c.gridy = 2;
		for(int i = 0; i < 5; i++){
			c.gridx = i;
			drsniki.addElement(new Drsnik(Drsnik.VERTICAL));
			drsniki.elementAt(i).setPreferredSize(new Dimension(30, 90));
			drsniki.elementAt(i).setEnabled(false);
			drsniki.elementAt(i).setVisible(true);
			switch(i){
			case 0: drsniki.elementAt(i).nastaviMeje(-180, 180);
			break;
			case 1: drsniki.elementAt(i).nastaviMeje(-500, 500);
			break;
			case 2: drsniki.elementAt(i).nastaviMeje(10, 200);
			break;
			case 3: drsniki.elementAt(i).nastaviMeje(1, 50);
			break;
			case 4: drsniki.elementAt(i).nastaviMeje(10, 100);
			break;
			}
			add(drsniki.elementAt(i), c);
		}
			
	}
	private void nastaviNapise(){
		c.gridy = 1;
		for(int i = 0; i < 5; i++){
			c.gridx = i;
			switch(i){
				case 0:add(new JLabel("theta"), c);
				break;
				case 1:add(new JLabel("omega"), c);
				break;
				case 2:add(new JLabel("l"), c);
				break;
				case 3:add(new JLabel("r"), c);
				break;
				case 4:add(new JLabel("masa"), c);
				break;
			}
		}
	}
	//Napisi pod drsniki.
	public void nastaviVrednostiNapise(){
		c.gridy = 3;
		for(int i = 0; i < 5; i++){
			c.gridx = i;
			switch(i){
				case 0:vrednosti.addElement(new JLabel());
				break;
				case 1:vrednosti.addElement(new JLabel());
				break;
				case 2:vrednosti.addElement(new JLabel());
				break;
				case 3:vrednosti.addElement(new JLabel());
				break;
				case 4:vrednosti.addElement(new JLabel());
				break;
			}
			add(vrednosti.elementAt(i), c);
			
		}
		
	}
	
	//Nastavi vrednost napisov pod drsniki.
	public void nastaviVrednosti(Nihalo nihalo){
		
		vrednosti.elementAt(0).setText(String.format("%1$,.2f" , new Double(nihalo.getTheta())));
		vrednosti.elementAt(1).setText(String.format("%1$,.2f" , new Double(nihalo.getOmega())));
		vrednosti.elementAt(2).setText(String.format("%1$,.0f" , new Double(nihalo.getDolzina())));
		vrednosti.elementAt(3).setText(String.format("%1$,.0f" , new Double(nihalo.getR())));
		vrednosti.elementAt(4).setText(String.format("%1$,.0f" , new Double(nihalo.getMasa())));
		
		
	}
	
	public Vector<Drsnik> getDrsniki() {
		return drsniki;
	}
		

}
