package nihalo.okno;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import nihalo.risanje.Nihalo;

/**
 * @author Urban
 * Razred, ki skrbi za pritiske miske in premikanje nihala z misko.
 */
public class MyMouseListener implements MouseListener, MouseMotionListener {
	
	private Platno platno;
	double xPavza;
	double yPavza;
	double velikostPavza;
	double xRestart;
	double yRestart;
	double velikostRestart;
	double xIzbira;
	double yIzbira;
	double x2Izbira;
	double y2Izbira;
	double velikostIzbira;

	public MyMouseListener(Platno platno) {
		this.platno = platno;
		nastavi();
	}
	
	public void nastavi(){
		
		xPavza = platno.getPavza().getX();
		yPavza = platno.getPavza().getY();
		velikostPavza = platno.getPavza().getVelikost();
		xRestart = platno.getRestart().getX();
		yRestart = platno.getRestart().getY();
		velikostRestart = platno.getRestart().getVelikost();
		xIzbira = platno.getIzbiraMetode().getX();
		yIzbira = platno.getIzbiraMetode().getY();;
		x2Izbira = platno.getIzbiraMetode().getX2();;
		y2Izbira = platno.getIzbiraMetode().getY2();;
		velikostIzbira = platno.getIzbiraMetode().getVelikost();;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	//Metoda, ki premika nihalo, kadar ga vlecemo naokoli
	@Override
	public void mousePressed(MouseEvent arg0) {
		for(Nihalo nihalo : platno.getNihala()){
		
			double r = nihalo.getR();
			//Razdalja med tocko kjer smo pritisnili in srediscem nihala.
			double distance = (nihalo.getxCenter()-arg0.getX())*(nihalo.getxCenter()-arg0.getX())
					+ (nihalo.getyCenter()-arg0.getY())*(nihalo.getyCenter()-arg0.getY());
			
			if(distance < r*r){
				nihalo.pritisnjen = true;
			}
		}
		
	}
	
	//Metoda gleda, kjer spustimo misko.
	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		//Ali smo pritisnili oz. spustili pavzo?
		if((arg0.getX() < xPavza + velikostPavza/2 && arg0.getX() > xPavza - velikostPavza/2)
				&& (arg0.getY() < yPavza + velikostPavza/2 && arg0.getY() > yPavza - velikostPavza/2)){
			
			platno.getPavza().setPlay();
			
			for(int i = 0; i < 5; i++){
				platno.getSubPlatno1().getDrsniki().elementAt(i).toggle();
				platno.getSubPlatno2().getDrsniki().elementAt(i).toggle();
			}
		}
		//Ali smo pritisnili restart?
		if((arg0.getX() < xRestart + velikostRestart/2 && arg0.getX() > xRestart - velikostRestart/2)
				&& (arg0.getY() < yRestart + velikostRestart/2 && arg0.getY() > yRestart - velikostRestart/2)){
			
			platno.getRestart().restart();
			

		}
		//Ali smo izbrali metodo Runge-Kutta 4?
		if((arg0.getX() < xIzbira + 0.6*velikostIzbira/2 && arg0.getX() > xIzbira - velikostIzbira/2)
				&& (arg0.getY() < yIzbira + 0.3*velikostIzbira && arg0.getY() > yIzbira - 0.2*velikostIzbira)){
			
			platno.getIzbiraMetode().izberiMetodoRunge();
		}
		//Ali smo izbrali metodo Euler?
		if((arg0.getX() < x2Izbira + 0.6*velikostIzbira/2 && arg0.getX() > x2Izbira - velikostIzbira/2)
				&& (arg0.getY() < y2Izbira + 0.3*velikostIzbira && arg0.getY() > y2Izbira - 0.2*velikostIzbira)){
			
			platno.getIzbiraMetode().izberiMetodoEuler();
		}
			
		
		//Ce spustimo misko, ne drzimo vec nihala.
		for(Nihalo nihalo : platno.getNihala()){
			nihalo.pritisnjen = false;
		}
		
		
	}

	//Metoda, ki omogoca vlecenje nihala naokoli.
	@Override
	public void mouseDragged(MouseEvent arg0) {
		for(Nihalo nihalo : platno.getNihala()){
			if(nihalo.pritisnjen && !platno.getPavza().getPlay()){
				nihalo.setTheta(Math.atan2(-nihalo.getxPos() + arg0.getX(), -nihalo.getyPos()+arg0.getY() ));
				platno.getNihala().elementAt(1).setxPos(platno.getNihala().elementAt(0).getxCenter());
				platno.getNihala().elementAt(1).setyPos(platno.getNihala().elementAt(0).getyCenter());
				if(nihalo.getRed() == 0){
					platno.getSubPlatno1().nastaviVrednosti(nihalo);
				}
				else{
					platno.getSubPlatno2().nastaviVrednosti(nihalo);
				}
				
				
			}
		}
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}
	

}
