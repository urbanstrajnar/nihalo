package nihalo.okno;

import java.awt.Color;
import java.awt.Graphics;

import nihalo.risanje.Nihalo;

public class Restart {
	
	private double x;
	private double y;
	private int[] xPoints;
	private int[] yPoints;
	private int velikost;
	private Platno platno;

	public Restart(Platno platno) {
		super();
		x = 50;
		y = 415;
		xPoints = new int[3];
		yPoints = new int[3];
		velikost = 60;
		nastaviTocke(velikost);
		this.platno = platno;
	}
	
	

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getVelikost() {
		return velikost;
	}	
	public void narisi(Graphics g){
		
			
			g.setColor(Color.blue);
			g.fillOval((int)(x-velikost/2.0), (int)(y-velikost/2.0), velikost, velikost);
			g.setColor(Color.white);
			g.fillOval((int)(x-0.35*velikost), (int)(y-0.35*velikost),(int) (0.7*velikost), (int) (0.7*velikost));
			g.setColor(Color.black);
			g.drawOval((int)(x-velikost/2.0), (int)(y-velikost/2.0), velikost, velikost);
			g.drawOval((int)(x-0.35*velikost), (int)(y-0.35*velikost),(int) (0.7*velikost), (int) (0.7*velikost));
			g.setColor(Color.white);
			g.fillRect((int)(x-0.1*velikost), (int)y,(int) (0.2*velikost), (int) (velikost));
			g.setColor(Color.black);
			g.drawLine((int)(x+0.1*velikost), (int)(y + 0.49*velikost), (int)(x+0.1*velikost), (int)(y+0.35*velikost));
			
			g.setColor(Color.blue);
			g.fillPolygon(xPoints, yPoints, 3);
			g.setColor(Color.black);
			g.drawPolygon(xPoints, yPoints, 3);
			g.setColor(Color.blue);
			g.drawLine(xPoints[1], (int)(y+0.5*velikost), xPoints[1], (int)(y+0.31*velikost));
			

			
	}
	
	private void nastaviTocke(int d){
		xPoints[0] = (int)(x +0.02*d);
		yPoints[0] = (int)(y + 0.425*d);
		xPoints[1] = (int)(x - 0.15*d);
		yPoints[1] = (int)(y + 0.58* d);
		xPoints[2] = (int)(x - 0.15*d);
		yPoints[2] = (int)(y + 0.225*d);
			
	}
	
	public void restart(){
		for(Nihalo nihalo: platno.getNihala()){
			nihalo.setOmega(0);
			nihalo.setTheta(Math.PI/2.0+Math.random()*Math.PI);
			nihalo.setxPos(300);
			nihalo.setyPos(100);
			nihalo.resetCrta();
			nihalo.setDolzina(100);
			nihalo.setMasa(50);
			nihalo.setR(10);

			if(nihalo.getRed() == 1){
				nihalo.setxPos(platno.getNihala().firstElement().getxCenter());
				nihalo.setyPos(platno.getNihala().firstElement().getyCenter());
			}
			
		}
		System.out.println("restart");
		
	}

}
