package nihalo.okno;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author Urban
 * Razred, ki sluzi kot gumb za pavzo.
 */
public class Pavza {
	
	private double x;
	private double y;
	private int[] xPoints;
	private int[] yPoints;
	private boolean play;
	private int velikost;

	public double getX() {
		return x;
	}



	public double getY() {
		return y;
	}



	public int getVelikost() {
		return velikost;
	}



	public Pavza() {
		super();
		x = 50;
		y = 350;
		xPoints = new int[3];
		yPoints = new int[3];
		play = true;
		velikost = 50;
		nastaviTocke(velikost);
		
	}
	
	
	
	public void narisi(Graphics g){
		
		if(play){
			
			g.setColor(Color.blue);
			g.fillRect((int)x - velikost/2, (int)y - velikost/2, (int)(3.0/8.0*velikost), velikost);
			g.fillRect((int)x + (int)(0.125*velikost), (int)y - velikost/2, (int)(3.0/8.0*velikost), velikost);
			g.setColor(Color.black);
			g.drawRect((int)x - velikost/2, (int)y - velikost/2, (int)(3.0/8.0*velikost), velikost);
			g.drawRect((int)x + (int)(0.125*velikost), (int)y - velikost/2, (int)(3.0/8.0*velikost), velikost);
			
		}
		
		else{
			
			g.setColor(Color.blue);
			g.fillPolygon(xPoints, yPoints, 3);
			g.setColor(Color.black);
			g.drawPolygon(xPoints, yPoints, 3);
			
		}
	}
	
	private void nastaviTocke(int d){
		xPoints[0] = (int) x + d/2;
		yPoints[0] = (int) y;
		xPoints[1] = (int) x - d/2;
		yPoints[1] = (int) y + d/2;
		xPoints[2] = (int) x - d/2;
		yPoints[2] = (int) y - d/2;
			
	}
	
	public void setPlay(){
		play = !play;
		
	}
	public boolean getPlay(){
		return play;
	}

}
