package nihalo.okno;

import java.awt.Color;
import java.awt.Graphics;

public class IzbiraMetode {
	
	private double x;
	private double y;
	private double x2;
	private double y2;
	private int velikost;
	private int metoda;

	public IzbiraMetode() {
		super();
		x = 35;
		y = 470;
		x2 = 75;
		y2 = 470;
		velikost = 60;
		metoda = 0;
	}
	
	

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	public double getX2() {
		return x2;
	}

	public double getY2() {
		return y2;
	}

	public int getVelikost() {
		return velikost;
	}	
	
	//metoda, ki narise dva gumba.
	public void narisi(Graphics g){
		
			if(metoda == 0){
				g.setColor(Color.yellow);
			}
			else{
				g.setColor(Color.lightGray);
			}
			g.fillRect((int)(x - 0.5*velikost), (int)(y - 0.2*velikost), (int)(0.6*velikost), (int)(0.3*velikost));
			if(metoda == 0){
				g.setColor(Color.lightGray);
			}
			else{
				g.setColor(Color.yellow);
			}
			g.fillRect((int)(x2 - 0.5*velikost), (int)(y2 - 0.2*velikost), (int)(0.6*velikost), (int)(0.3*velikost));
			g.setColor(Color.black);
			g.drawRect((int)(x - 0.5*velikost), (int)(y - 0.2*velikost), (int)(0.6*velikost), (int)(0.3*velikost));
			g.drawRect((int)(x2 - 0.5*velikost), (int)(y2 - 0.2*velikost), (int)(0.6*velikost), (int)(0.3*velikost));
			g.drawString("RK4", (int)(x-0.38*velikost),(int)(y + 0.02 * velikost));
			g.drawString("Euler", (int)(x2-0.41*velikost), (int) (y2 + 0.02 * velikost));
			

			
	}
	
	
	public void izberiMetodoEuler(){
		
		metoda = 1;
		
	}
	public void izberiMetodoRunge(){
		
		metoda = 0;
		
	}
	
	public int getMetoda(){
		return metoda;
	}

}