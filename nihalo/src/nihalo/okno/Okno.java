package nihalo.okno;

import java.awt.HeadlessException;

import javax.swing.JFrame;

/**
 * @author Urban
 *Razred Okno je okno, ki vsebuje vse ostale komponente. 
 */
@SuppressWarnings("serial")
public class Okno extends JFrame{
	
	
	private Platno platno;


	public Okno(String arg0) throws HeadlessException {
		super(arg0);
		
		platno = new Platno();
		add(platno);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}
	
	
	public Platno getPlatno(){
		return platno;
	}
	

	
	
}

